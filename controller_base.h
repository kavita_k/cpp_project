#ifndef Controller_base_H
#define Controller_base_H
#include "model.h"
#include "view.h"

class Controller_base 
{
	public:
	virtual ~Controller_base() { }
	virtual void update() = 0;
	virtual void add(View* ptr_v) = 0;

};

#endif