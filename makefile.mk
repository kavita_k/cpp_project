a: model.o model1.o controller_base.o controller.o view_sq.o view.o client.o
	g++ model.o model1.o controller_base.o controller.o view_sq.o view.o client.o

client.o: client.cpp model.h model1.h controller_base.h controller.h view_sq.h view.h
	g++ -c client.cpp 
model.o:model.cpp model.h
	g++ -c model.cpp
model1.o:model1.cpp model1.h
	g++ -c model1.cpp	
view_sq.o:view_sq.cpp view_sq.h
	g++ -c view_sq.cpp

view.o:view.cpp view.h
	g++ -c view.cpp

controller_base.o:controller_base.cpp controller_base.h
	g++ -c controller_base.cpp

controller.o:controller.cpp controller.h
	g++ -c controller.cpp

