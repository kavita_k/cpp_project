#ifndef view_sq_h
#define view_sq_h
#include "view.h"
#include "controller_base.h"


class View_sq : public View
{
	public:
	//TAKING DATA FROM MODEL TO DISPLAY AS OUTPUT
	virtual void update();
	virtual void get_model(Model *ptr_model);
	virtual void register_view(Controller_base* ptr_c);
	 
	private:
	Model *ptr_model_;

};

#endif