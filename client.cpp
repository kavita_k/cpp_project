#include <iostream>
using namespace std;
#include "model.h"
#include "controller.h"
#include "controller_base.h"
#include "model1.h"
#include "view.h"
#include "view_sq.h"

int main()
{
	Model1 m(10);
	Controller c(&m);
	m.set_controller(&c);
	
	View_sq view_sq;
	//view_sq.register_view(&c);
	view_sq.get_model(&m);

    view_sq.update();
    
    m.change(20);
	view_sq.update();

    m.change(30);
    view_sq.update();
}