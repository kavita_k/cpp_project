#ifndef MODEL1_H
#define MODEL1_H
#include "model.h"
#include "controller_base.h"

class Model1 : public Model 
{
	public:
	Model1(int );
	virtual void set_controller(Controller_base* ptr_c);
    void change(int x) ;
	int get_x();
	private:
	Controller_base* ptr_c_;
	int x_;
	
}; 
#endif

