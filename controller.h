#ifndef Controller_H
#define Controller_H
#include "controller_base.h"

class Controller : public Controller_base
{
	public:
	Controller(Model* ptr_m);
	virtual void add(View* ptr_v);
	virtual void update();


	private:
	Model *ptr_m_;
	View *ptr_v1_;

};

#endif